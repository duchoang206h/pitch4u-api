const httpStatus = require('http-status');
const { voucherService } = require('../services');
const catchAsync = require('../utils/catchAsync');

const getVoucherByPitch = catchAsync(async (req, res) => {
  const { pitchId } = req.query;
  const vouchers = await voucherService.getVoucherByPitch(pitchId);
  res.status(httpStatus.OK).send({ result: vouchers });
});
module.exports = {
  getVoucherByPitch,
};

module.exports.authController = require('./auth.controller');
module.exports.userController = require('./user.controller');
module.exports.pitchController = require('./pitch.controller');
module.exports.uploadController = require('./upload.controller');
module.exports.postController = require('./post.controller');
module.exports.bookingController = require('./booking.controller');
module.exports.voucherController = require('./voucher.controller');
module.exports.notificationController = require('./notification.controller');

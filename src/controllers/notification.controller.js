const httpStatus = require('http-status');
const { notificationService } = require('../services');
const catchAsync = require('../utils/catchAsync');

const getNotifications = catchAsync(async (req, res) => {
  const { page = 1, limit = 20 } = req.query;
  const userId = req.user.user_id;
  const notifications = await notificationService.getNotificationPagination(
    { user_id: userId },
    { page: parseInt(page), limit: parseInt(limit) }
  );
  res.status(httpStatus.OK).send({ result: notifications });
});
const getUnReadCount = catchAsync(async (req, res) => {
  const count = await notificationService.getUnReadCount(req.user.user_id);
  res.status(httpStatus.OK).send({ result: count });
});
const updateRead = catchAsync(async (req, res) => {
  const count = await notificationService.update(
    {
      user_id: req.user.user_id,
      notification_id: req.params.notificationId,
    },
    {
      is_read: true,
    }
  );
  res.status(httpStatus.OK).send({ result: count });
});
module.exports = {
  getNotifications,
  getUnReadCount,
  updateRead,
};

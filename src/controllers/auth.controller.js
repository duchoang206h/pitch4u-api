const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { authService, userService, tokenService, emailService, roleService } = require('../services');
const { USER_PROVIDER, ROLES } = require('../config/constant');
const { hashPassword } = require('../utils/hash');
const { OKJson } = require('../utils/common');

const register = catchAsync(async (req, res) => {
  const createBody = {
    ...req.body,
    provider: USER_PROVIDER.LOCAL,
    role_id: (await roleService.getRoleByName(ROLES.USER)).role_id,
  };
  // check exist user with email and not verified
  const existUser = await userService.getUserByEmail(createBody.email, false);
  const user = existUser || (await userService.createUser(createBody));
  const tokens = await tokenService.generateAuthTokens(user);
  await authService.sendVerifyEmail(user);
  res.status(httpStatus.CREATED).send({ user, tokens });
});
const setupPassword = catchAsync(async (req, res) => {
  const { password } = req.body;
  await userService.updateUserById(req.user.user_id, { password: await hashPassword(password) }, { is_verified: true });
  res.status(httpStatus.OK).send();
});

const login = catchAsync(async (req, res) => {
  const { email, password } = req.body;
  const user = await authService.loginUserWithEmailAndPassword(email, password);
  const tokens = await tokenService.generateAuthTokens(user);
  res.send({ user, tokens });
});

const logout = catchAsync(async (req, res) => {
  await authService.logout(req.body.refresh_token);
  res.status(httpStatus.OK).send(OKJson);
});

const refreshTokens = catchAsync(async (req, res) => {
  const tokens = await authService.refreshAuth(req.body.refresh_token);
  res.status(httpStatus.OK).send({ ...tokens });
});

const forgotPassword = catchAsync(async (req, res) => {
  const { email } = req.body;
  const user = await userService.getUserByEmail(email);
  if (!user) return res.status(httpStatus.NOT_FOUND).send({});
  await authService.sendEmailResetPassword(user);
  res.status(httpStatus.OK).send(OKJson);
});
const verifyForgotPassword = catchAsync(async (req, res) => {
  const { code, email } = req.body;
  await authService.verifyForgotPassword(email, code);
  res.status(httpStatus.OK).send(OKJson);
});
const setForgotPassword = catchAsync(async (req, res) => {
  const { email, code, password } = req.body;
  await authService.forgotPassword({ email, code, password });
  res.status(httpStatus.OK).send(OKJson);
});
const resetPassword = catchAsync(async (req, res) => {
  const { password, new_password } = req.body;
  await authService.resetPassword(req.user.user_id, { password, new_password });
  res.status(httpStatus.OK).send(OKJson);
});
const verifyEmail = catchAsync(async (req, res) => {
  await authService.verifyEmail(req.user.user_id, req.body.code);
  res.status(httpStatus.OK).send(OKJson);
});
const resendVerifyEmail = catchAsync(async (req, res) => {
  await authService.sendVerifyEmail(req.user);
  res.status(httpStatus.OK).send(OKJson);
});
module.exports = {
  register,
  login,
  logout,
  refreshTokens,
  forgotPassword,
  resetPassword,
  verifyEmail,
  resendVerifyEmail,
  setupPassword,
  verifyForgotPassword,
  setForgotPassword,
};

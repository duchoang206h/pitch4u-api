const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const uploadUtils = require('../utils/upload');

const uploadImage = catchAsync(async (req, res) => {
  const { file } = req;
  const url = await uploadUtils.uploadImage(file.buffer);
  return res.status(httpStatus.OK).send({ result: url });
});
module.exports = {
  uploadImage,
};

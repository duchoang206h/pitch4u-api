const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { pitchService } = require('../services');
const { PITCH_TYPES } = require('../config/constant');

const registerPitch = catchAsync(async (req, res) => {
  const { user_id } = req.user;
  const pr = await pitchService.registerPitch(user_id, req.body);
  res.status(httpStatus.CREATED).send({ result: pr });
});
const getPitchesPagination = catchAsync(async (req, res) => {
  const { limit = 10, page = 1, name = '', address = '', rate_lte = 5, rate_gte = 0 } = req.query;
  const result = await pitchService.getPitchesPagination(
    { name, address, rate_gte: parseFloat(rate_gte), rate_lte: parseFloat(rate_lte) },
    { limit: parseInt(limit, 10), page: parseInt(page, 10) }
  );
  res.status(httpStatus.OK).send({ result });
});
const approvePitch = catchAsync(async (req, res) => {
  const { user_id } = req.user;
  const { registration_id } = req.body;
  const pr = await pitchService.approvePitchRegistration(user_id, registration_id);
  res.status(httpStatus.OK).send({ result: pr });
});
const getPitchesRegistrationPagination = catchAsync(async (req, res) => {
  const { limit = 10, page = 1, name = '', address = '', status, sort = 'desc', sort_by = 'createdAt' } = req.query;
  const pr = await pitchService.getPitchesRegistrationPagination(
    { name, address, status, rate_gte: parseFloat(rate_gte), rate_lte: parseFloat(rate_lte) },
    { limit: parseInt(limit, 10), page: parseInt(page, 10) },
    { sort, sort_by }
  );
  res.status(httpStatus.OK).send({ result: pr });
});

const getPitchBySlug = catchAsync(async (req, res) => {
  const { slug } = req.params;
  const pitch = await pitchService.getPitch({ slug });
  res.status(httpStatus.OK).send({ result: pitch });
});
const addSubPitch = catchAsync(async (req, res) => {
  const { pitch_id, name, price, type } = req.body;
  const subpitch = await pitchService.addSubPitch({ pitch_id, name, price, type });
  res.status(httpStatus.OK).send({ result: subpitch });
});
const pitchType = catchAsync(async (_, res) => {
  res.status(httpStatus.OK).send({ result: PITCH_TYPES });
});
const getPitchDetail = catchAsync(async (req, res) => {
  const detail = await pitchService.getDetail(req.query.pitch_id);
  res.status(httpStatus.OK).send({ result: detail });
});
const getBookingStatus = catchAsync(async (req, res) => {
  const bookingStatus = await pitchService.getBookingStatus(req.query.pitch_id);
  res.status(httpStatus.OK).send({ result: bookingStatus });
});
const getMyPitchesPagination = catchAsync(async (req, res) => {
  const { limit = 10, page = 1, name = '', address = '', status } = req.query;
  const pitches = await pitchService.getPitchesPagination(
    { name, address, status, user_id: req.user.user_id },
    { limit: parseInt(limit), page: parseInt(page) }
  );
  res.status(httpStatus.OK).send({ result: pitches });
});
module.exports = {
  registerPitch,
  getPitchesPagination,
  approvePitch,
  getPitchesRegistrationPagination,
  getPitchBySlug,
  addSubPitch,
  pitchType,
  getPitchDetail,
  getBookingStatus,
  getMyPitchesPagination,
};

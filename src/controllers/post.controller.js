const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const uploadUtils = require('../utils/upload');
const postService = require('../services/post.service');
const { OKJson } = require('../utils/common');
const createPost = catchAsync(async (req, res) => {
  const { hashtag = '', images = [], text = '' } = req.body;
  const post = await postService.createPost(req.user.user_id, { hashtag, images, text });
  return res.status(httpStatus.OK).send({ result: post });
});
const getPosts = catchAsync(async (req, res) => {
  const { page = 1, limit = 10, text = '', hashtag = '', scope = 'all' } = req.query;
  const posts = await postService.getPostPagination(
    { hashtag, text, scope, userId: req.user.user_id },
    {
      limit: parseInt(limit),
      page: parseInt(page),
    }
  );
  return res.status(httpStatus.OK).send({ result: posts });
});
const likePost = catchAsync(async (req, res) => {
  const { post_id } = req.body;
  await postService.likePost(req.user.user_id, post_id);
  return res.status(httpStatus.OK).send(OKJson);
});
const commentPost = catchAsync(async (req, res) => {
  const { post_id } = req.body;
  const { text } = req.body;
  const comment = await postService.commentPost(req.user.user_id, post_id, text);
  return res.status(httpStatus.OK).send({ result: comment });
});
const getPostDetail = catchAsync(async (req, res) => {
  const { postId } = req.params;
  const post = await postService.getPostDetail(postId);
  if (!post) return res.status(httpStatus.NOT_FOUND).send();
  return res.status(httpStatus.OK).send({ result: post });
});
const replyCommentPost = catchAsync(async (req, res) => {
  const { post_id, parent_id, text } = req.body;
  const comment = await postService.replyCommentPost({ user_id: req.user.user_id, post_id, parent_id, text });
  return res.status(httpStatus.OK).send({ result: comment });
});
module.exports = {
  createPost,
  getPosts,
  likePost,
  commentPost,
  getPostDetail,
  replyCommentPost,
};

const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const { bookingService } = require('../services');
const { PAYMENT_TYPE } = require('../config/constant');
const { createPaymentUrl, validatePaymentReturn } = require('../services/payment/vnpay');

const booking = catchAsync(async (req, res) => {
  // eslint-disable-next-line camelcase
  const { subpitch_id, start_time, end_time, payment_type, voucher_id } = req.body;
  const bookingRs = await bookingService.booking({
    subpitch_id,
    start_time,
    end_time,
    user_id: req.user.user_id,
    payment_type,
    voucher_id,
    ipAddr:
      req.headers['x-forwarded-for'] ||
      req.connection.remoteAddress ||
      req.socket.remoteAddress ||
      req.connection.socket.remoteAddress,
  });
  // eslint-disable-next-line camelcase

  res.status(200).json({
    result: bookingRs,
  });
});
const vnpPaymentReturn = catchAsync(async (req, res) => {
  const data = req.query;
  const {
    vnp_Amount,
    vnp_BankCode,
    vnp_BankTranNo,
    vnp_CardType,
    vnp_OrderInfo,
    vnp_PayDate,
    vnp_ResponseCode,
    vnp_TmnCode,
    vnp_TransactionNo,
    vnp_TransactionStatus,
    vnp_TxnRef,
  } = data;
  if (validatePaymentReturn(data)) {
    await bookingService.validateTransactionReturn({
      booking_id: parseInt(vnp_TxnRef),
      amount: parseFloat(vnp_Amount) / 100,
      tx_code: vnp_TransactionNo,
      extra_data: {
        vnp_BankCode,
        vnp_BankTranNo,
        vnp_CardType,
        vnp_OrderInfo,
        vnp_PayDate,
        vnp_ResponseCode,
        vnp_TmnCode,
        vnp_TransactionStatus,
      },
    });
  }
  return res.status(200).send({});
});
const paymentType = (_, res) => res.status(200).json({ result: PAYMENT_TYPE });
const getUserBookings = catchAsync(async (req, res) => {
  const { page = 1, limit = 10 } = req.query;
  const bookings = await bookingService.getUserBookings(req.user.user_id, { limit: parseInt(limit), page: parseInt(page) });
  res.status(200).json({ result: bookings });
});
const getBookingPaginate = catchAsync(async (req, res) => {
  const { limit = 10, page = 1, name = '', start_date = null, end_date = null } = req.query;
  const bookings = await bookingService.getBookingPaginate({
    limit,
    page,
    name,
    start_date,
    end_date,
  });
  res.status(200).json({
    result: bookings,
  });
});
module.exports = {
  booking,
  paymentType,
  vnpPaymentReturn,
  getUserBookings,
  getBookingPaginate,
};

const Sequelize = require('sequelize');
const { Base } = require('./Base');

class Like extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
    Like.belongsTo(models.Post, {
      as: 'post',
      foreignKey: 'post_id',
      targetKey: 'post_id',
    });
    Like.belongsTo(models.Comment, {
      as: 'comment',
      foreignKey: 'comment_id',
      targetKey: 'comment_id',
    });
    Like.belongsTo(models.User, {
      as: 'user_like',
      foreignKey: 'user_id',
      targetKey: 'user_id',
    });
  }

  static init(sequelize) {
    return super.init(
      {
        like_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        post_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        user_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        comment_id: {
          type: Sequelize.INTEGER,
          allowNull: true,
        },
        type: {
          type: Sequelize.STRING,
          allowNull: false,
        },
      },
      {
        sequelize,
        modelName: 'Like',
        freezeTableName: true,
        tableName: 'likes',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  Like,
};

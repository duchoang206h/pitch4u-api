const Sequelize = require('sequelize');
const { Base } = require('./Base');

class ChatMember extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
    ChatMember.belongsTo(models.Chat, { foreignKey: 'chat_id', targetKey: 'chat_id', as: 'chat' });
  }

  static init(sequelize) {
    return super.init(
      {
        member_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        chat_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        user_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
      },
      {
        sequelize,
        modelName: 'ChatMember',
        freezeTableName: true,
        tableName: 'chat_members',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  ChatMember,
};

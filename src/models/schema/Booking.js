const Sequelize = require('sequelize');
const { Base } = require('./Base');

class Booking extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
    Booking.hasMany(models.BookingPitch, {
      as: 'booking_pitches',
      foreignKey: 'booking_id',
      sourceKey: 'booking_id',
    });
    Booking.belongsTo(models.Pitch, {
      as: 'pitch',
      foreignKey: 'pitch_id',
      targetKey: 'pitch_id',
    });
    Booking.belongsTo(models.User, {
      as: 'user',
      foreignKey: 'user_id',
      targetKey: 'user_id',
    });
  }

  static init(sequelize) {
    return super.init(
      {
        booking_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        user_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        payment_type: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        status: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        discount: {
          type: Sequelize.FLOAT,
          allowNull: false,
          defaultValue: 0,
        },
        total: {
          type: Sequelize.FLOAT,
          allowNull: false,
        },
        voucher_id: {
          type: Sequelize.INTEGER,
          allowNull: true,
        },
        tournament_id: {
          type: Sequelize.INTEGER,
          allowNull: true,
        },
        pitch_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
      },
      {
        sequelize,
        modelName: 'Booking',
        freezeTableName: true,
        tableName: 'bookings',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  Booking,
};

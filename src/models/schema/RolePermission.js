const Sequelize = require('sequelize');
const { Base } = require('./Base');

class RolePermission extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
  }

  static init(sequelize) {
    return super.init(
      {
        role_id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
        },
        permission_id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
        },
      },
      {
        sequelize,
        modelName: 'RolePermission',
        freezeTableName: true,
        tableName: 'role_permissions',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  RolePermission,
};

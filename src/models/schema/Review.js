const Sequelize = require('sequelize');
const { Base } = require('./Base');

class Review extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
    Review.belongsTo(models.Pitch, {
      as: 'pitch',
      foreignKey: 'pitch_id',
      targetKey: 'pitch_id',
    });
  }

  static init(sequelize) {
    return super.init(
      {
        review_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        pitch_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        user_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        text: {
          type: Sequelize.TEXT,
          allowNull: false,
          defaultValue: '',
        },
        star: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
      },
      {
        sequelize,
        modelName: 'Review',
        freezeTableName: true,
        tableName: 'reviews',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  Review,
};

const Sequelize = require('sequelize');
const { Base } = require('./Base');

class Transaction extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
  }

  static init(sequelize) {
    return super.init(
      {
        transaction_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        booking_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        total: {
          type: Sequelize.FLOAT,
          allowNull: false,
        },
        tx_code: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        status: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        error: {
          type: Sequelize.STRING,
          allowNull: false,
          defaultValue: '',
        },
        data: {
          type: Sequelize.JSON,
          allowNull: false,
          defaultValue: '{}',
        },
        type: {
          type: Sequelize.STRING,
        },
      },
      {
        sequelize,
        modelName: 'Transaction',
        freezeTableName: true,
        tableName: 'transactions',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  Transaction,
};

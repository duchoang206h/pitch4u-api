const Sequelize = require('sequelize');
const { Base } = require('./Base');

class Chat extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
    Chat.belongsToMany(models.User, {
      as: 'members',
      through: {
        model: models.ChatMember,
      },
      foreignKey: 'chat_id',
    });
    Chat.hasMany(models.ChatMember, {
      as: 'chat_members',
      foreignKey: 'chat_id',
      sourceKey: 'chat_id',
    });
  }

  static init(sequelize) {
    return super.init(
      {
        chat_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        type: {
          type: Sequelize.STRING,
        },
        created_by: {
          type: Sequelize.INTEGER,
        },
      },
      {
        sequelize,
        modelName: 'Chat',
        freezeTableName: true,
        tableName: 'chats',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  Chat,
};

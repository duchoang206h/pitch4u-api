const Sequelize = require('sequelize');
const { Base } = require('./Base');

class Pitch extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
    Pitch.hasOne(models.PitchConfig, {
      as: 'config',
      foreignKey: 'pitch_id',
      sourceKey: 'pitch_id',
    });
    Pitch.hasMany(models.SubPitch, {
      as: 'sub_pitches',
      foreignKey: 'pitch_id',
      sourceKey: 'pitch_id',
    });
    Pitch.hasMany(models.Booking, {
      as: 'bookings',
      foreignKey: 'pitch_id',
      sourceKey: 'pitch_id',
    });
    Pitch.hasMany(models.Review, {
      as: 'reviews',
      foreignKey: 'pitch_id',
      sourceKey: 'pitch_id',
    });
    Pitch.belongsTo(models.User, {
      foreignKey: 'user_id',
      targetKey: 'user_id',
      as: 'user',
    });
  }

  static init(sequelize) {
    return super.init(
      {
        pitch_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        name: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        slug: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        address: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        logo: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        suspended: {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: false,
        },
        long: {
          type: Sequelize.FLOAT,
          allowNull: true,
        },
        lat: {
          type: Sequelize.FLOAT,
          allowNull: true,
        },
        user_id: {
          type: Sequelize.INTEGER,
        },
      },
      {
        sequelize,
        modelName: 'Pitch',
        freezeTableName: true,
        tableName: 'pitches',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  Pitch,
};

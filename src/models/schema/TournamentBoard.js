const Sequelize = require('sequelize');
const { Base } = require('./Base');

class TournamentBoard extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {}

  static init(sequelize) {
    return super.init(
      {
        board_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        tournament_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },

        name: {
          type: Sequelize.STRING,
          allowNull: false,
        },
      },
      {
        sequelize,
        modelName: 'TournamentBoard',
        freezeTableName: true,
        tableName: 'tournament_boards',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  TournamentBoard,
};

const Sequelize = require('sequelize');
const { Base } = require('./Base');

class PostMedia extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
    PostMedia.belongsTo(models.Post, {
      as: 'post',
      foreignKey: 'post_id',
      targetKey: 'post_id',
    });
  }

  static init(sequelize) {
    return super.init(
      {
        media_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        post_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        type: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        path: {
          type: Sequelize.STRING,
          allowNull: true,
        },
      },
      {
        sequelize,
        modelName: 'PostMedia',
        freezeTableName: true,
        tableName: 'post_medias',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  PostMedia,
};

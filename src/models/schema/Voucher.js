const Sequelize = require('sequelize');
const { Base } = require('./Base');

class Voucher extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
  }

  static init(sequelize) {
    return super.init(
      {
        voucher_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        code: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        type: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        active: {
          type: Sequelize.BOOLEAN,
          allowNull: false,
        },
        expire_date: {
          type: Sequelize.DATE,
          allowNull: false,
        },
        limit_time: {
          type: Sequelize.INTEGER,
          allowNull: true,
        },
        value: {
          type: Sequelize.FLOAT,
          allowNull: false,
        },
        max_amount: {
          type: Sequelize.INTEGER,
          allowNull: true,
        },
        created_from: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        created_by: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        pitch_id: {
          type: Sequelize.INTEGER,
          allowNull: true,
        },
      },
      {
        sequelize,
        modelName: 'Voucher',
        freezeTableName: true,
        tableName: 'vouchers',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  Voucher,
};

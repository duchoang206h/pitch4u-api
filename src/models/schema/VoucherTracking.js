const Sequelize = require('sequelize');
const { Base } = require('./Base');

class VoucherTracking extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
  }

  static init(sequelize) {
    return super.init(
      {
        voucher_tracking_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        voucher_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        booking_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        user_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
      },
      {
        sequelize,
        modelName: 'VoucherTracking',
        freezeTableName: true,
        tableName: 'voucher_trackings',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  VoucherTracking,
};

const Sequelize = require('sequelize');
const { Base } = require('./Base');

class Role extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
    Role.hasMany(models.User, {
      foreignKey: 'role_id',
      sourceKey: 'role_id',
      as: 'users',
    });
    Role.belongsToMany(models.Permission, {
      through: models.RolePermission,
      foreignKey: 'role_id',
      otherKey: 'permission_id',
      as: 'permissions',
    });
  }

  static init(sequelize) {
    return super.init(
      {
        role_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        name: {
          type: Sequelize.STRING,
          allowNull: false,
        },
      },
      {
        sequelize,
        modelName: 'Role',
        freezeTableName: true,
        tableName: 'roles',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  Role,
};

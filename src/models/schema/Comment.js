const Sequelize = require('sequelize');
const { Base } = require('./Base');

class Comment extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
    Comment.belongsTo(models.Post, {
      as: 'post',
      foreignKey: 'post_id',
      targetKey: 'post_id',
    });
    Comment.hasMany(models.Like, {
      as: 'comment_likes',
      foreignKey: 'comment_id',
      sourceKey: 'comment_id',
    });
    Comment.hasMany(models.Comment, {
      as: 'replies',
      foreignKey: 'parent_id',
      sourceKey: 'comment_id',
    });
    Comment.belongsTo(models.User, {
      as: 'user_comment',
      foreignKey: 'user_id',
      targetKey: 'user_id',
    });
  }

  static init(sequelize) {
    return super.init(
      {
        comment_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        post_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        user_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        text: {
          type: Sequelize.TEXT,
          allowNull: true,
        },
        like_count: {
          type: Sequelize.INTEGER,
          allowNull: false,
          defaultValue: 0,
        },
        parent_id: {
          type: Sequelize.INTEGER,
          allowNull: true,
        },
      },
      {
        sequelize,
        modelName: 'Comment',
        freezeTableName: true,
        tableName: 'comments',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  Comment,
};

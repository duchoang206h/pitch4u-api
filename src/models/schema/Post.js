const Sequelize = require('sequelize');
const { Base } = require('./Base');

class Post extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
    Post.hasMany(models.PostMedia, {
      as: 'media',
      foreignKey: 'post_id',
      sourceKey: 'post_id',
    });
    Post.hasMany(models.Like, {
      as: 'likes',
      foreignKey: 'post_id',
      sourceKey: 'post_id',
    });
    Post.hasMany(models.Comment, {
      as: 'comments',
      foreignKey: 'post_id',
      sourceKey: 'post_id',
    });
  }

  static init(sequelize) {
    return super.init(
      {
        post_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        user_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        text: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        hashtag: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        like_count: {
          type: Sequelize.INTEGER,
          allowNull: false,
          defaultValue: 0,
        },
        comment_count: {
          type: Sequelize.INTEGER,
          allowNull: false,
          defaultValue: 0,
        },
      },
      {
        sequelize,
        modelName: 'Post',
        freezeTableName: true,
        tableName: 'posts',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  Post,
};

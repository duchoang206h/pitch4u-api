const Sequelize = require('sequelize');
const { Base } = require('./Base');

class Permission extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
    Permission.belongsToMany(models.Role, {
      through: models.RolePermission,
      foreignKey: 'permission_id',
      otherKey: 'role_id',
      as: 'roles',
    });
  }

  static init(sequelize) {
    return super.init(
      {
        permission_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        name: {
          type: Sequelize.STRING,
          allowNull: false,
        },
      },
      {
        sequelize,
        modelName: 'Permission',
        freezeTableName: true,
        tableName: 'permissions',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  Permission,
};

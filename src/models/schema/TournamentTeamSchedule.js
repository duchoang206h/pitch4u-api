const Sequelize = require('sequelize');
const { Base } = require('./Base');

class TournamentTeamSchedule extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {}

  static init(sequelize) {
    return super.init(
      {
        schedule_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        tournament_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        host_team_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        guest_team_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        round: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        subpitch_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        start_time: {
          type: Sequelize.FLOAT,
          allowNull: false,
        },
        end_time: {
          type: Sequelize.FLOAT,
          allowNull: false,
        },
        price: {
          type: Sequelize.FLOAT,
          allowNull: false,
        },
      },
      {
        sequelize,
        modelName: 'TournamentTeamSchedule',
        freezeTableName: true,
        tableName: 'tournament_team_schedules',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  TournamentTeamSchedule,
};

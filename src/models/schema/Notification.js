const Sequelize = require('sequelize');
const { Base } = require('./Base');

class Notification extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
  }

  static init(sequelize) {
    return super.init(
      {
        notification_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        user_id: {
          type: Sequelize.INTEGER,
          allowNull: true,
        },
        data: {
          type: Sequelize.JSON,
          allowNull: false,
        },
        is_read: {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: false,
        },
        type: {
          type: Sequelize.STRING,
          allowNull: false,
        },
      },
      {
        sequelize,
        modelName: 'Notification',
        freezeTableName: true,
        tableName: 'notifications',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  Notification,
};

const Sequelize = require('sequelize');
const { Base } = require('./Base');

class PitchRegistration extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
  }

  static init(sequelize) {
    return super.init(
      {
        registration_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        user_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        pitch_name: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        pitch_address: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        fullname: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        card_id: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        email: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        phone: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        address: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        status: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        approve_by: {
          type: Sequelize.INTEGER,
        },
        long: {
          type: Sequelize.FLOAT,
          allowNull: false,
        },
        lat: {
          type: Sequelize.FLOAT,
        },
      },
      {
        sequelize,
        modelName: 'PitchRegistration',
        freezeTableName: true,
        tableName: 'pitch_registrations',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  PitchRegistration,
};

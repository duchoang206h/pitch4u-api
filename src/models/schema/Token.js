const Sequelize = require('sequelize');
const { Base } = require('./Base');

class Token extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
    Token.belongsTo(models.User, {
      foreignKey: 'user_id',
      targetKey: 'user_id',
      as: 'user',
    });
  }

  static init(sequelize) {
    return super.init(
      {
        token_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        token: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        user_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        type: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        expires: {
          type: Sequelize.DATE,
          required: true,
        },
        blacklisted: {
          type: Sequelize.BOOLEAN,
          defaultValue: false,
        },
      },
      {
        sequelize,
        modelName: 'Token',
        freezeTableName: true,
        tableName: 'tokens',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  Token,
};

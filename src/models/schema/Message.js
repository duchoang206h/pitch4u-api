const Sequelize = require('sequelize');
const { Base } = require('./Base');

class Message extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
  }

  static init(sequelize) {
    return super.init(
      {
        message_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        chat_id: {
          type: Sequelize.STRING, /// private, group
          allowNull: false,
        },
        user_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        text: {
          type: Sequelize.STRING,
        },
        is_read: {
          type: Sequelize.BOOLEAN,
          defaultValue: false,
        },
      },
      {
        sequelize,
        modelName: 'Message',
        freezeTableName: true,
        tableName: 'messages',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  Message,
};

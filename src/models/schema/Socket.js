const Sequelize = require('sequelize');
const { Base } = require('./Base');

class Socket extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    Socket.belongsTo(models.User, {
      as: 'user',
      foreignKey: 'user_id',
      targetKey: 'user_id',
    });
  }

  static init(sequelize) {
    return super.init(
      {
        id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        user_id: {
          type: Sequelize.INTEGER,
        },
        socket_id: {
          type: Sequelize.STRING,
        },
      },
      {
        sequelize,
        modelName: 'Socket',
        freezeTableName: true,
        tableName: 'sockets',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  Socket,
};

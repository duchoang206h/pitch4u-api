const Sequelize = require('sequelize');
const { Base } = require('./Base');

class SubPitch extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
    SubPitch.belongsTo(models.Pitch, {
      as: 'pitch',
      targetKey: 'pitch_id',
      foreignKey: 'pitch_id',
    });
    SubPitch.hasMany(models.BookingPitch, {
      as: 'booking_pitches',
      foreignKey: 'subpitch_id',
      sourceKey: 'subpitch_id',
    });
  }

  static init(sequelize) {
    return super.init(
      {
        subpitch_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        pitch_id: {
          type: Sequelize.INTEGER,
        },
        name: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        price: {
          type: Sequelize.FLOAT,
          allowNull: false,
        },
        type: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        active: {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: true,
        },
      },
      {
        sequelize,
        modelName: 'SubPitch',
        freezeTableName: true,
        tableName: 'subpitches',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  SubPitch,
};

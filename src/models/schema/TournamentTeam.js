const Sequelize = require('sequelize');
const { Base } = require('./Base');

class TournamentTeam extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {}

  static init(sequelize) {
    return super.init(
      {
        team_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        tournament_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        board_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        name: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        logo: {
          type: Sequelize.STRING,
          allowNull: true,
        },
      },
      {
        sequelize,
        modelName: 'TournamentTeam',
        freezeTableName: true,
        tableName: 'tournament_teams',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  TournamentTeam,
};

const Sequelize = require('sequelize');
const { Base } = require('./Base');

class BookingPitch extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
    BookingPitch.belongsTo(models.Booking, {
      as: 'booking',
      foreignKey: 'booking_id',
      targetKey: 'booking_id',
    });
    BookingPitch.belongsTo(models.SubPitch, {
      as: 'sub_pitch',
      foreignKey: 'subpitch_id',
      targetKey: 'subpitch_id',
    });
  }

  static init(sequelize) {
    return super.init(
      {
        booking_pitch_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        booking_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        subpitch_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        start_time: {
          type: Sequelize.DATE,
          allowNull: false,
        },
        end_time: {
          type: Sequelize.DATE,
          allowNull: false,
        },
        price: {
          type: Sequelize.FLOAT,
          allowNull: false,
        },
      },
      {
        sequelize,
        modelName: 'BookingPitch',
        freezeTableName: true,
        tableName: 'booking_pitches',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  BookingPitch,
};

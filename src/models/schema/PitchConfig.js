const Sequelize = require('sequelize');
const { Base } = require('./Base');

class PitchConfig extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
    PitchConfig.belongsTo(models.Pitch, {
      as: 'pitch',
      foreignKey: 'pitch_id',
      targetKey: 'pitch_id',
    });
  }

  static init(sequelize) {
    return super.init(
      {
        pitch_config_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        pitch_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        open_at: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        close_at: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        time_frames: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        open_days: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        active: {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: true,
        },
      },
      {
        sequelize,
        modelName: 'PitchConfig',
        freezeTableName: true,
        tableName: 'pitch_configs',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  PitchConfig,
};

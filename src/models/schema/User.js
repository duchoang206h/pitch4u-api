const Sequelize = require('sequelize');
const { Base } = require('./Base');

class User extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
    User.hasMany(models.Pitch, {
      foreignKey: 'user_id',
      sourceKey: 'user_id',
      as: 'pitches',
    });
    User.hasMany(models.Token, {
      foreignKey: 'user_id',
      sourceKey: 'user_id',
      as: 'tokens',
    });
    User.hasOne(models.Socket, {
      as: 'socket',
      foreignKey: 'user_id',
      sourceKey: 'user_id',
    });
    User.belongsTo(models.Role, {
      foreignKey: 'role_id',
      targetKey: 'role_id',
      as: 'role',
    });
    User.belongsToMany(models.Chat, {
      as: 'chats',
      through: {
        model: models.ChatMember,
      },
      foreignKey: 'user_id',
    });
  }

  static init(sequelize) {
    return super.init(
      {
        user_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        fullname: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        phone: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        email: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        provider: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        password: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        is_verified: {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: false,
        },
        avatar: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        gender: {
          type: Sequelize.STRING,
          allowNull: true,
        },
        birthday: {
          type: Sequelize.DATEONLY,
          allowNull: true,
        },
        role_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
      },
      {
        sequelize,
        modelName: 'User',
        freezeTableName: true,
        tableName: 'users',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
User.commonAttributes = ['user_id', 'fullname', 'avatar'];
module.exports = {
  User,
};

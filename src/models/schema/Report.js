const Sequelize = require('sequelize');
const { Base } = require('./Base');

class Report extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
  }

  static init(sequelize) {
    return super.init(
      {
        report_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        pitch_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        user_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        text: {
          type: Sequelize.TEXT,
          allowNull: false,
          defaultValue: '',
        },
        attaches: {
          type: Sequelize.STRING,
          allowNull: false,
        },
      },
      {
        sequelize,
        modelName: 'Report',
        freezeTableName: true,
        tableName: 'reports',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  Report,
};

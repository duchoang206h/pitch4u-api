const Sequelize = require('sequelize');
const { Base } = require('./Base');

class Resource extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {
    // define association here
  }

  static init(sequelize) {
    return super.init(
      {
        resource_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        name: {
          type: Sequelize.STRING,
          allowNull: false,
        },
      },
      {
        sequelize,
        modelName: 'Resource',
        freezeTableName: true,
        tableName: 'resources',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  Resource,
};

const Sequelize = require('sequelize');
const { Base } = require('./Base');

class Tournament extends Base {
  /**
   * Helper method for defining associations.
   * This method is not a part of Sequelize lifecycle.
   * The `models/index` file will call this method automatically.
   */
  static associate(models) {}

  static init(sequelize) {
    return super.init(
      {
        tournament_id: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
        },
        pitch_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        name: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        type: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        num_team: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        num_board: {
          type: Sequelize.INTEGER,
          allowNull: true,
        },
        user_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
      },
      {
        sequelize,
        modelName: 'Tournament',
        freezeTableName: true,
        tableName: 'tournaments',
        timestamps: true,
        paranoid: true,
      }
    );
  }
}
module.exports = {
  Tournament,
};

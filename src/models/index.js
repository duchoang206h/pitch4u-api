const { Sequelize } = require('sequelize');
const { Booking } = require('./schema/Booking');
const { BookingPitch } = require('./schema/BookingPitch');
const { Comment } = require('./schema/Comment');
const { Like } = require('./schema/Like');
const { Notification } = require('./schema/Notification');
const { Permission } = require('./schema/Permission');
const { Pitch } = require('./schema/Pitch');
const { PitchConfig } = require('./schema/PitchConfig');
const { PitchRegistration } = require('./schema/PitchRegistration');
const { Post } = require('./schema/Post');
const { PostMedia } = require('./schema/PostMedia');
const { Report } = require('./schema/Report');
const { Resource } = require('./schema/Resource');
const { Review } = require('./schema/Review');
const { Role } = require('./schema/Role');
const { RolePermission } = require('./schema/RolePermission');
const { SubPitch } = require('./schema/SubPitch');
const { Token } = require('./schema/Token');
const { Tournament } = require('./schema/Tournament');
const { TournamentBoard } = require('./schema/TournamentBoard');
const { TournamentTeam } = require('./schema/TournamentTeam');
const { TournamentTeamSchedule } = require('./schema/TournamentTeamSchedule');
const { Transaction } = require('./schema/Transaction');
const { User } = require('./schema/User');
const { Voucher } = require('./schema/Voucher');
const { VoucherTracking } = require('./schema/VoucherTracking');
const { VerificationCode } = require('./schema/VerificationCode');
const { Message } = require('./schema/Message');
const { Chat } = require('./schema/Chat');
const { ChatMember } = require('./schema/ChatMember');
const { Socket } = require('./schema/Socket');

const config = require('../config/config');

const models = {
  Booking,
  BookingPitch,
  Comment,
  Like,
  Notification,
  Permission,
  Pitch,
  PitchConfig,
  PitchRegistration,
  Post,
  PostMedia,
  Report,
  Resource,
  Review,
  Role,
  RolePermission,
  SubPitch,
  Token,
  Tournament,
  TournamentBoard,
  TournamentTeam,
  TournamentTeamSchedule,
  Transaction,
  User,
  Voucher,
  VoucherTracking,
  VerificationCode,
  Chat,
  ChatMember,
  Message,
  Socket,
};
const sequelize = new Sequelize(config.mysql.url, {
  dialect: 'mysql',
  logging: true,
  pool: {
    max: 10,
    min: 0,
    acquire: 60000,
    idle: 20000,
  },
});
Object.keys(models).forEach((x) => {
  models[x].init(sequelize);
});
Object.keys(models).forEach((x) => {
  models[x].associate(models);
});
module.exports = {
  ...models,
  sequelize,
};

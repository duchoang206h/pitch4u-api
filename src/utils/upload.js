const config = require('../config/config');
const FormData = require('form-data');
const { request } = require('./request');

const uploadImage = async (buffer) => {
  let url = '';
  const formData = new FormData();
  formData.append('image', buffer);
  const headers = { Authorization: `Client-ID ${config.imgur.clientID}` };
  const response = await request('POST', config.imgur.apiUrl, formData, headers);
  if (response && response.status === 200) url = response.data.link;
  return url;
};
module.exports = {
  uploadImage,
};

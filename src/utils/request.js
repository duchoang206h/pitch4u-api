const { default: axios } = require('axios');
const request = async (method, url, body = {}, headers = {}) => {
  const { data } = await axios({
    method,
    url,
    data: body,
    headers,
  });
  return data;
};
module.exports = {
  request,
};

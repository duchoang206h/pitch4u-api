function getDaysInMonth(month, year) {
  // Month argument is 0-based in JavaScript (0 - January, 11 - December)
  return new Date(year, month + 1, 0).getDate();
}

function generateDaysArray() {
  const today = new Date();
  const currentMonth = today.getMonth();
  const currentYear = today.getFullYear();
  const daysInMonth = getDaysInMonth(currentMonth, currentYear);
  const daysArray = [];

  for (let i = today.getDate(); i <= daysInMonth; i++) {
    daysArray.push(i);
  }

  return daysArray;
}
const getMonth = () => {
  const today = new Date();
  const currentMonth = today.getMonth() + 1;
  return currentMonth;
};
const getYear = () => {
  const today = new Date();
  const currentYear = today.getFullYear();
  return currentYear;
};
module.exports = {
  generateDaysArray,
  getMonth,
  getYear,
};

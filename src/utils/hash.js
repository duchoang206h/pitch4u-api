const bcrypt = require('bcryptjs');
const SALT = 10;
const hashPassword = (pw) => bcrypt.hash(pw, SALT);
const verifyPassword = (pw, hash) => bcrypt.compare(pw, hash);
const genCode = (length = 6) => Math.random().toString().substr(2, length);
module.exports = {
  hashPassword,
  verifyPassword,
  genCode,
};

const slugify = require('slugify');
const getOffset = (page, limit) => {
  return Number(page - 1) * Number(limit);
};
const slug = (text) =>
  slugify(text, {
    replacement: '-', // replace spaces with replacement character, defaults to `-`
    remove: undefined, // remove characters that match regex, defaults to `undefined`
    lower: true, // convert to lower case, defaults to `false`
    strict: false, // strip special characters except replacement, defaults to `false`
    locale: 'vi', // language code of the locale to use
    trim: true, // trim leading and trailing replacement chars, defaults to `true`
  });
const OKJson = {
  message: 'OK',
};
module.exports = {
  getOffset,
  slug,
  OKJson,
};

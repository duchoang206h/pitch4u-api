/* eslint-disable no-console */
const chatService = require('../services/chat.service');
function handleMessage(socket, io) {
  const { user } = socket;
  socket.on('message', async ({ chatId, text }) => {
    if (!(await chatService.checkUserInChat(socket.user.user_id, chatId))) return;
    const message = await chatService.createMessage({ chat_id: chatId, text, user_id: user.user_id });
    io.to(`room:chat_${chatId}`).emit('message', message);
  });
}
function handleLoadListChat(socket, io) {
  socket.on('load_chats', async () => {
    const chats = await chatService.getListChatUser(socket.user.user_id);
    io.to(`room:user_${socket.user.user_id}`).emit('load_chats', chats);
  });
}
function handleLoadMessage(socket, io) {
  socket.on('load_messages', async ({ chatId, page = 1, limit = 10 }) => {
    if (!(await chatService.checkUserInChat(socket.user.user_id, chatId))) return;
    const chats = await chatService.getMessagePagination(
      { chat_id: chatId, user_id: socket.user.user_id },
      { page: parseInt(page, 10), limit: parseInt(limit, 10) }
    );
    io.to(`room:user_${socket.user.user_id}`).emit('load_messages', chats);
  });
}
function handleReadMessage(socket, io) {
  socket.on('read_message', async ({ messageId }) => {
    const message = await chatService.getMessage({ messageId });
    if (!message) return;
    await chatService.updateReadMessage({ messageId, userId: socket.user.user_id });
    io.to(`room:chat_${message.chat_id}`).emit('read_message', message);
  });
}
function handleJoinRoomChat(socket, io) {
  socket.on('join_chat', async ({ chatId }) => {
    if (!(await chatService.checkUserInChat(socket.user.user_id, chatId))) return;
    socket.join(`room:chat_${chatId}`);
  });
}
function handleCreateChat(socket, io) {
  socket.on('create_chat', async ({ userId }) => {
    await chatService.createChat([socket.user.user_id, userId]);
  });
}
module.exports = {
  handleMessage,
  handleLoadListChat,
  handleLoadMessage,
  handleReadMessage,
  handleJoinRoomChat,
  handleCreateChat,
};

/* eslint-disable no-console */

const auth = require('../middlewares/autSocket');
const {
  handleLoadMessage,
  handleLoadListChat,
  handleJoinRoomChat,
  handleReadMessage,
  handleMessage,
  handleCreateChat,
} = require('./chat');

function initializeSocket(server) {
  // eslint-disable-next-line global-require
  const io = require('socket.io')(server, {
    cors: {
      origin: '*',
    },
  });
  io.use(auth);
  io.on('connection', async (socket) => {
    socket.join(`room:user_${socket.user.user_id}`);
    handleCreateChat(socket, io);
    handleMessage(socket, io);
    handleLoadListChat(socket, io);
    handleLoadMessage(socket, io);
    handleReadMessage(socket, io);
    handleJoinRoomChat(socket, io);
  });
  return io;
}

module.exports = initializeSocket;

const httpStatus = require('http-status');
const { ROLES } = require('../config/constant');
const { Role } = require('../models');
const { pitchService } = require('../services');

const owner = async (req, res, next) => {
  try {
    const { pitch_id } = { ...req.body, ...req.query, ...req.params };
    const userRole = await Role.findByPk(req.user.role_id);
    if (userRole && userRole.name === ROLES.ADMIN) return next();
    const pitch = await pitchService.getPitch({ pitch_id, user_id: req.user.user_id });
    if (!pitch)
      return res.status(httpStatus.BAD_REQUEST).json({
        message: 'You are not owner of the pitch',
      });
    return next();
  } catch (error) {
    return res.status(httpStatus.INTERNAL_SERVER_ERROR).json();
  }
};
module.exports = {
  owner,
};

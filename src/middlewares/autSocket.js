/* eslint-disable no-param-reassign */
// socket-auth.js
const jwt = require('jsonwebtoken');
const config = require('../config/config');
const { User } = require('../models');

function socketAuthentication(socket, next) {
  // Extract the token from the query parameters
  const { authorization } = socket.handshake.headers;
  if (!authorization) return next(new Error('Authentication failed.'));
  const token = authorization.split(' ')[1];
  // Verify the token
  jwt.verify(token, config.jwt.secret, async (err, payload) => {
    if (err) {
      return next(new Error('Authentication failed.'));
    }

    try {
      // Find the user based on the decoded user ID from the token
      const user = await User.findOne({ where: { user_id: payload.sub }, raw: true });

      if (!user) {
        return next(new Error('User not found.'));
      }

      // Add the user object to the socket
      socket.user = user;

      // Continue with the connection
      return next();
    } catch (error) {
      return next(error);
    }
  });
}

module.exports = socketAuthentication;

const Joi = require('joi');

const registerPitch = {
  body: Joi.object().keys({
    pitch_name: Joi.string().required(),
    pitch_address: Joi.string().required(),
    fullname: Joi.string().required(),
    card_id: Joi.string().required(),
    email: Joi.string().required(),
    phone: Joi.string().required(),
    address: Joi.string().required(),
    lat: Joi.number(),
    long: Joi.number(),
  }),
};
const approvePitch = {
  body: Joi.object().keys({
    registration_id: Joi.number().required(),
  }),
};
const addSubPitch = {
  body: Joi.object().keys({
    pitch_id: Joi.number().required(),
    name: Joi.string().required(),
    type: Joi.string().required(),
    price: Joi.number().min(0).required(),
  }),
};
const bookingStatus = {
  query: Joi.object().keys({
    pitch_id: Joi.number().required(),
  }),
};
module.exports = {
  registerPitch,
  approvePitch,
  addSubPitch,
  bookingStatus,
};

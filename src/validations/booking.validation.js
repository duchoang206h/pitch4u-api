const Joi = require('joi');
const booking = {
  body: Joi.object().keys({
    subpitch_id: Joi.number().required(),
    start_time: Joi.date().required(),
    end_time: Joi.date().required(),
    payment_type: Joi.string().required(),
    voucher_id: Joi.number(),
  }),
};
module.exports = {
  booking,
};

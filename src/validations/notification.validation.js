const Joi = require('joi');
const updateRead = {
  params: Joi.object().keys({
    notificationId: Joi.number().required(),
  }),
};
module.exports = {
  updateRead,
};

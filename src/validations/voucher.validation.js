const Joi = require('joi');

const getVoucher = {
  query: Joi.object().keys({
    pitchId: Joi.number().required(),
  }),
};
module.exports = {
  getVoucher,
};

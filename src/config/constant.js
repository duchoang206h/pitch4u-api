const USER_PROVIDER = {
  LOCAL: 'local',
};
const ROLES = {
  USER: 'user',
  ADMIN: 'admin',
  SUPER_ADMIN: 'super_admin',
};
const REGISTER_PITCH_STATUS = {
  PENDING: 'pending',
  APPROVE: 'approved',
  DENY: 'denied',
};
const BOOKING_STATUS = {
  PENDING: 'pending',
  SUCCESS: 'success',
};
const PAYMENT_TYPE = {
  VNPAY: 'vnpay',
  PAY_LATER: 'pay_later',
};
const TRANSACTION_STATUS = {
  PENDING: 'pending',
  SUCCESS: 'success',
  ERROR: 'error',
};
const NOTIFICATION_TYPES = {
  SYSTEM: 'system',
  POST_COMMENT: 'post_comment',
  POST_LIKE: 'post_like',
  POST_REPLY_COMMENT: 'post_reply_comment',
};
const PITCH_TYPES = {
  PITCH5: 'PITCH5',
  PITCH7: 'PITCH7',
  PITCH9: 'PITCH9',
  PITCH11: 'PITCH11',
};
const VOUCHER_TYPES = {
  FIXED: 'fixed',
  PERCENT: 'percent',
};
module.exports = {
  USER_PROVIDER,
  ROLES,
  REGISTER_PITCH_STATUS,
  NOTIFICATION_TYPES,
  PITCH_TYPES,
  BOOKING_STATUS,
  PAYMENT_TYPE,
  TRANSACTION_STATUS,
  VOUCHER_TYPES,
};

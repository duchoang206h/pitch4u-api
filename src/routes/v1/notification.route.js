const express = require('express');
const validate = require('../../middlewares/validate');
const notificationValidation = require('../../validations/notification.validation');
const auth = require('../../middlewares/auth');
const { notificationController } = require('../../controllers');

const router = express.Router();
router.get('/', auth('user'), notificationController.getNotifications);
router.get('/unread', auth('user'), notificationController.getUnReadCount);
router.patch(
  '/:notificationId',
  validate(notificationValidation.updateRead),
  auth('user'),
  notificationController.updateRead
);
module.exports = router;

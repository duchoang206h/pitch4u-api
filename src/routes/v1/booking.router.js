const express = require('express');
const validate = require('../../middlewares/validate');
const bookingValidation = require('../../validations/booking.validation');
const auth = require('../../middlewares/auth');
const { bookingController } = require('../../controllers');

const router = express.Router();
router.post('/', auth('user'), validate(bookingValidation.booking), bookingController.booking);
router.get('/my-bookings', auth('user'), bookingController.getUserBookings);

router.get('/payment-type', bookingController.paymentType);
router.get('/vnp/return', bookingController.vnpPaymentReturn);

// admin
router.get('/', auth('admin'), bookingController.getBookingPaginate);
router.get('/', auth('admin'), bookingController.getBookingPaginate);

module.exports = router;

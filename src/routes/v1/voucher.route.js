const express = require('express');
const validate = require('../../middlewares/validate');
const voucherValidation = require('../../validations/voucher.validation');
const voucherController = require('../../controllers/voucher.controller');
const auth = require('../../middlewares/auth');

const router = express.Router();
router.get('/', auth('user'), validate(voucherValidation.getVoucher), voucherController.getVoucherByPitch);
module.exports = router;

const express = require('express');
const { postController } = require('../../controllers');
const auth = require('../../middlewares/auth');
const router = express.Router();
router.post('/', auth('user'), postController.createPost);
router.get('/', auth('user'), postController.getPosts);
router.get('/:postId', auth('user'), postController.getPostDetail);
router.post('/like', auth('user'), postController.likePost);
router.post('/comment', auth('user'), postController.commentPost);
router.post('/comment/reply', auth('user'), postController.replyCommentPost);

module.exports = router;

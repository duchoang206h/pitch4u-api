const express = require('express');
const authRoute = require('./auth.route');
const userRoute = require('./user.route');
const docsRoute = require('./docs.route');
const pitchRoute = require('./pitch.route');
const uploadRoute = require('./upload.router');
const postRoute = require('./post.router');
const bookingRoute = require('./booking.router');
const voucherRoute = require('./voucher.route');
const notificationRoute = require('./notification.route');

const config = require('../../config/config');

const router = express.Router();

const defaultRoutes = [
  {
    path: '/auth',
    route: authRoute,
  },
  {
    path: '/users',
    route: userRoute,
  },
  {
    path: '/pitches',
    route: pitchRoute,
  },
  {
    path: '/upload',
    route: uploadRoute,
  },
  {
    path: '/posts',
    route: postRoute,
  },
  {
    path: '/booking',
    route: bookingRoute,
  },
  {
    path: '/vouchers',
    route: voucherRoute,
  },
  {
    path: '/notifications',
    route: notificationRoute,
  },
];

const devRoutes = [
  // routes available only in development mode
  {
    path: '/docs',
    route: docsRoute,
  },
];

defaultRoutes.forEach((route) => {
  router.use(route.path, route.route);
});

/* istanbul ignore next */
if (config.env === 'development') {
  devRoutes.forEach((route) => {
    router.use(route.path, route.route);
  });
}

module.exports = router;

const express = require('express');
const uploadMiddleware = require('../../middlewares/upload');
const { uploadController } = require('../../controllers');
const router = express.Router();
router.post('/image', uploadMiddleware.single('image'), uploadController.uploadImage);
module.exports = router;

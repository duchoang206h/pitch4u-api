const { Op } = require('sequelize');
const { Notification } = require('../models');
const { getOffset } = require('../utils/common');

const createNotification = async (data) => {
  const notification = await Notification.create(data);
  return notification;
};
const getNotificationPagination = async (filter, { page, limit } = { limit: 10, page: 1 }) => {
  const { user_id, type } = filter;
  const filterAnd = [];
  if (user_id) filterAnd.push({ user_id });
  if (type) filterAnd.push({ type });
  const notifications = await Notification.findAndCountAll({
    where: {
      [Op.and]: filterAnd,
    },
    limit,
    offset: getOffset(page, limit),
  });
  return {
    data: notifications.rows,
    pagination: {
      total: notifications.count,
      page,
      limit,
    },
  };
};
const update = async (filter, data) => Notification.update(data, { where: filter });
const getUnReadCount = async (userId) => Notification.count({ where: { user_id: userId, is_read: false } });
module.exports = {
  getNotificationPagination,
  createNotification,
  getUnReadCount,
  update,
};

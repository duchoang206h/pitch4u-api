const { Op } = require('sequelize');
const { Chat, User, sequelize, Message, ChatMember, Pitch } = require('../models');
const { getOffset } = require('../utils/common');

const getListChatUser = async (userId) => {
  const sql = `SELECT c.chat_id
  FROM chats c
  INNER JOIN chat_members cm ON c.chat_id = cm.chat_id
  LEFT JOIN messages m ON c.chat_id = m.chat_id
  WHERE cm.user_id = $1
  GROUP BY c.chat_id
  ORDER BY MAX(m.createdAt) DESC;`;
  const [listChatIds] = await sequelize.query(sql, { bind: [userId] });
  let chats = await Chat.findAll({
    where: {
      chat_id: { [Op.in]: listChatIds.map((c) => c.chat_id) },
    },
    include: [
      {
        model: User,
        as: 'members',
        attributes: User.commonAttributes,
        include: [
          {
            model: Pitch,
            as: 'pitches',
            required: false,
          },
        ],
      },
    ],
  });
  chats = await Promise.all(
    chats.map(async (chat) => {
      chat = chat.toJSON();
      chat.last_message = await Message.findOne({ where: { chat_id: chat.chat_id }, order: [['createdAt', 'desc']] });
      chat.unread_message_count = await Message.count({
        where: {
          chat_id: chat.chat_id,
          user_id: {
            [Op.not]: userId,
          },
          is_read: false,
        },
      });
      return chat;
    })
  );
  return chats;
};
const createMessage = async (data) => Message.create(data);
const getMessagePagination = async (filter, pagination) => {
  // eslint-disable-next-line camelcase
  const { chat_id, text } = filter;
  const { page, limit } = pagination;
  const filterAnd = [];
  // eslint-disable-next-line camelcase
  if (chat_id) filterAnd.push({ chat_id });

  if (text)
    filterAnd.push({
      text: {
        [Op.like]: `%${text}%`,
      },
    });

  const messages = await Message.findAndCountAll({
    where: {
      [Op.and]: filterAnd,
    },
    limit,
    offset: getOffset(page, limit),
    order: [['createdAt', 'desc']],
  });
  return {
    data: messages.rows,
    total: messages.count,
    page,
    limit,
  };
};
const getMessage = async ({ messageId }) =>
  Message.findOne({
    where: {
      message_id: messageId,
    },
    raw: true,
  });
const updateReadMessage = async ({ messageId, userId }) => {
  return Message.update(
    { is_read: true },
    {
      where: {
        message_id: {
          [Op.lte]: messageId,
        },
        user_id: {
          [Op.ne]: userId,
        },
      },
    }
  );
};
const getChat = async (filter) => Chat.findOne({ where: filter });
const checkUserInChat = async (userId, chatId) => {
  const member = await ChatMember.findOne({
    where: {
      user_id: userId,
      chat_id: chatId,
    },
  });
  if (!member) return false;
  return true;
};
const createChat = async (userIds) => {
  const exist = await ChatMember.count({
    where: {
      user_id: {
        [Op.in]: userIds,
      },
    },
    group: ['chat_id'],
    having: sequelize.literal('COUNT(*) = 2'),
  });
  if (exist.length) return;
  return Chat.create(
    {
      createBy: userIds[0],
      chat_members: userIds.map((userId) => ({ user_id: userId })),
    },
    {
      include: [
        {
          model: ChatMember,
          as: 'chat_members',
        },
      ],
    }
  );
};
module.exports = {
  getListChatUser,
  createMessage,
  getMessagePagination,
  getMessage,
  updateReadMessage,
  getChat,
  checkUserInChat,
  createChat,
};

const { Sequelize, Op } = require('sequelize');
const { REGISTER_PITCH_STATUS, NOTIFICATION_TYPES } = require('../config/constant');
const { PitchRegistration, sequelize, Pitch, Post, PostMedia, Like, Comment, User } = require('../models');
const { getOffset, slug } = require('../utils/common');
const config = require('../config/config');
const { request } = require('../utils/request');
const { notificationService, userService } = require('.');
const ApiError = require('../utils/ApiError');

const getPostPagination = async (filter, paginate = { limit: 10, page: 1 }) => {
  const { text, scope, userId } = filter;
  const { limit, page } = paginate;
  const filterAnd = [];
  if (text) filterAnd.push({ text: { [Op.like]: `%${text}%` } });
  if (scope === 'me' && userId) filterAnd.push({ user_id: userId });
  const posts = await Post.findAndCountAll({
    where: {
      [Op.and]: filterAnd,
    },
    limit,
    offset: getOffset(page, limit),
    include: [
      {
        model: PostMedia,
        as: 'media',
      },
    ],
  });
  return {
    data: posts.rows,
    total: posts.count,
    page,
    limit,
  };
};

const createPost = async (userId, data) => {
  const { hashtag = '', text = '', images = [] } = data;
  const post = await Post.create(
    {
      user_id: userId,
      hashtag,
      text,
      media: images.map((img) => ({ type: 'image', path: img })),
    },
    {
      include: [
        {
          model: PostMedia,
          as: 'media',
        },
      ],
    }
  );
  return post;
};
const likePost = async (userId, postId) => {
  const t = await sequelize.transaction();
  try {
    const userPk = await userService.getUserPk(userId);
    const exist = await Like.findOne({
      where: {
        user_id: userId,
        post_id: postId,
        type: 'like',
      },
    });
    if (exist) throw new ApiError(400, 'Liked post');
    await Like.create(
      {
        user_id: userId,
        post_id: postId,
        type: 'like',
      },
      { transaction: t }
    );
    await Post.increment('like_count', { by: 1, where: { post_id: postId }, transaction: t });
    await t.commit();
    createPostNotification(`${userPk} đã thích bài viết của bạn`, {
      user_id: userId,
      post_id: postId,
      type: NOTIFICATION_TYPES.POST_LIKE,
    });
  } catch (error) {
    await t.rollback();
    throw error;
  }
};
const commentPost = async (userId, postId, text) => {
  const t = await sequelize.transaction();
  try {
    const userPk = await userService.getUserPk(userId);

    const comment = await Comment.create({
      user_id: userId,
      post_id: postId,
      text,
    });
    await Post.increment('comment_count', { by: 1, where: { post_id: postId }, transaction: t });
    await t.commit();
    createPostNotification(`${userPk} đã bình luận bài viết của bạn`, {
      user_id: userId,
      post_id: postId,
      comment_id: comment.comment_id,
      type: NOTIFICATION_TYPES.POST_COMMENT,
    });
    return comment;
  } catch (error) {
    await t.rollback();
    throw error;
  }
};
const getPostDetail = async (postId) => {
  const post = await Post.findOne({
    where: {
      post_id: postId,
    },
    include: [
      {
        model: PostMedia,
        as: 'media',
      },
      {
        model: Comment,
        as: 'comments',
        where: {
          parent_id: null,
        },
        attributes: ['text', 'like_count', 'comment_id', 'parent_id'],
        include: [
          {
            model: Comment,
            as: 'replies',
            attributes: ['text', 'like_count', 'comment_id', 'parent_id'],
            include: [
              {
                model: User,
                as: 'user_comment',
                attributes: User.commonAttributes,
              },
            ],
          },
          {
            model: User,
            as: 'user_comment',
            attributes: User.commonAttributes,
          },
        ],
      },
      {
        model: Like,
        as: 'likes',
        attributes: ['type'],
        include: [
          {
            model: User,
            as: 'user_like',
            attributes: User.commonAttributes,
          },
        ],
      },
    ],
  });
  return post;
};
const replyCommentPost = async ({ user_id, post_id, parent_id, text }) => {
  const t = await sequelize.transaction();
  try {
    const userPk = await userService.getUserPk(user_id);
    const parentComment = await Comment.findByPk(parent_id);
    const comment = await Comment.create({
      user_id,
      post_id,
      parent_id,
      text,
    });
    await Post.increment('comment_count', { by: 1, where: { post_id }, transaction: t });
    createPostNotification(`${userPk} đã trả lời bình luận bạn`, {
      user_id: parentComment.user_id,
      post_id,
      comment_id: comment.comment_id,
      type: NOTIFICATION_TYPES.POST_REPLY_COMMENT,
    });
    await t.commit();
    return comment;
  } catch (error) {
    await t.rollback();
    throw error;
  }
};
const createPostNotification = async (text, { user_id, post_id, type, comment_id = null }) => {
  try {
    const post = await Post.findByPk(post_id);
    if (post.user_id === user_id) return;
    const data = {
      text,
      user_id,
      post_id,
      comment_id,
    };
    await notificationService.createNotification({
      user_id: post.user_id,
      type,
      data,
    });
  } catch (error) {}
};
module.exports = {
  createPost,
  getPostPagination,
  likePost,
  commentPost,
  getPostDetail,
  replyCommentPost,
};

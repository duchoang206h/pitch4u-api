const { Sequelize, Op } = require('sequelize');
const { REGISTER_PITCH_STATUS, ROLES } = require('../config/constant');
const { PitchRegistration, sequelize, Pitch, SubPitch, PitchConfig, Role, User } = require('../models');
const { getOffset, slug } = require('../utils/common');
const ApiError = require('../utils/ApiError');
const { getMonth, getYear, generateDaysArray } = require('../utils/day');

/* ------------------------ service for user */
const registerPitch = (userId, data) => {
  const createData = {
    user_id: userId,
    status: REGISTER_PITCH_STATUS.PENDING,
    ...data,
  };
  return PitchRegistration.create(createData);
};
const getUserPitchRegistration = (userId) => {
  return PitchRegistration.findAll({ where: { user_id: userId } });
};
const getPitchRegistration = (pitchRegistrationId) => {
  return PitchRegistration.findOne({ where: { registration_id: pitchRegistrationId }, raw: true });
};
/* ------------------------ service for admin */
const approvePitchRegistration = async (adminId, pitchRegistrationId) => {
  const pr = await getPitchRegistration(pitchRegistrationId);
  const roleAdmin = await Role.findOne({ where: { name: ROLES.ADMIN } });
  if (!pr) throw Error();
  const t = await sequelize.transaction();
  try {
    await PitchRegistration.update(
      { status: REGISTER_PITCH_STATUS.APPROVE, approve_by: adminId },
      { where: { registration_id: pitchRegistrationId }, transaction: t }
    );
    await User.update({ role_id: roleAdmin.role_id }, { where: { user_id: pr.user_id }, transaction: t });
    await Pitch.create(
      {
        user_id: pr.user_id,
        name: pr.pitch_name,
        address: pr.pitch_address,
        long: pr.long,
        lat: pr.lat,
        slug: slug(pr.pitch_name),
        logo: null,
      },
      { transaction: t }
    );
    await t.commit();
    return;
  } catch (error) {
    await t.rollback();
    throw new Error();
  }
};
const getPitchesPagination = async (filter, paginate = { limit: 1, page: 1 }) => {
  // eslint-disable-next-line camelcase
  const { name, address, user_id, rate_gte, rate_lte } = filter;
  const { limit, page } = paginate;
  const filterAnd = [];
  const havingFilter = { rate: {} };
  if (name) filterAnd.push({ name: { [Op.like]: `%${name}%` } });
  if (address) filterAnd.push({ address: { [Op.like]: `%${address}%` } });
  // eslint-disable-next-line camelcase
  if (rate_gte) havingFilter.rate[Op.gte] = rate_gte;
  // eslint-disable-next-line camelcase
  if (rate_lte) havingFilter.rate[Op.lte] = rate_lte;
  // eslint-disable-next-line camelcase
  if (user_id) filterAnd.push({ user_id });
  const [pitches, count] = await Promise.all([
    Pitch.findAll({
      where: {
        [Op.and]: filterAnd,
      },
      limit,
      offset: getOffset(page, limit),
      attributes: {
        include: [
          [
            sequelize.fn(
              'COALESCE',
              sequelize.literal('(SELECT AVG(star) FROM reviews WHERE pitch_id = Pitch.pitch_id)'),
              0
            ),
            'rate',
          ],
          [sequelize.literal(`(select max(price) from subpitches where pitch_id = Pitch.pitch_id)`), 'max_price'],
          [sequelize.literal(`(select min(price) from subpitches where pitch_id = Pitch.pitch_id)`), 'min_price'],
        ],
      },
      having: havingFilter,
    }),
    //todo: optimize later
    Pitch.findAll({
      where: {
        [Op.and]: filterAnd,
      },
      attributes: {
        include: [
          [
            sequelize.fn(
              'COALESCE',
              sequelize.literal('(SELECT AVG(star) FROM reviews WHERE pitch_id = Pitch.pitch_id)'),
              0
            ),
            'rate',
          ],
        ],
      },
      having: havingFilter,
    }),
  ]);
  return {
    data: pitches,
    total: count.length,
    page,
    limit,
  };
};
const getPitchesRegistrationPagination = async (filter, paginate = { limit: 10, page: 1 }, { sort, sort_by }) => {
  const { name, address, status } = filter;
  const { limit, page } = paginate;
  const filterAnd = [];
  if (name) filterAnd.push({ pitch_name: { [Op.like]: `%${name}%` } });
  if (address) filterAnd.push({ pitch_name: { [Op.like]: `%${address}%` } });
  if (status) filterAnd.push({ status });
  // eslint-disable-next-line camelcase, no-param-reassign
  const registrations = await PitchRegistration.findAndCountAll({
    where: {
      [Op.and]: filterAnd,
    },
    limit,
    offset: getOffset(page, limit),
    // eslint-disable-next-line camelcase
    order: [[sort_by, sort]],
  });
  return {
    data: registrations.rows,
    total: registrations.count,
    page,
    limit,
  };
};
const getPitch = async (where) => Pitch.findOne({ where, raw: true });
const addSubPitch = async (data) => {
  const { name, pitch_id } = data;
  const exist = await SubPitch.findOne({ where: { name, pitch_id } });
  if (exist) throw new ApiError(400, 'Name existed');
  return SubPitch.create(data);
};
const getDetail = async (pitch_id) => {
  const detail = await Pitch.findOne({
    where: {
      pitch_id,
    },
    include: [
      {
        model: PitchConfig,
        as: 'config',
      },
      {
        model: SubPitch,
        as: 'sub_pitches',
      },
    ],
  });
  return detail;
};
const getBookingStatus = async (pitch_id) => {
  const sqlQuery = `SELECT
  DAY(bp.createdAt) AS day,
  HOUR(start_time) AS start_hour,
  HOUR(end_time) AS end_hour,
  bp.subpitch_id,
  COUNT(*) AS bookings_count
  FROM
  booking_pitches bp 
  INNER JOIN bookings b
    on b.booking_id = bp.booking_id
  WHERE
  b.pitch_id = ? AND
  MONTH(bp.createdAt) = ? AND
  YEAR(bp.createdAt) = ?
  GROUP BY
  DAY(bp.createdAt), start_hour, end_hour, bp.subpitch_id;
`;
  const config = await PitchConfig.findOne({ where: { pitch_id } });
  if (!config) throw new ApiError(400, 'Pitch has not been config for booking');
  const subpitches = await SubPitch.findAll({ where: { pitch_id }, raw: true });
  const timeFrames = JSON.parse(config.time_frames);
  const [bookings] = await sequelize.query(sqlQuery, {
    replacements: [pitch_id, getMonth(), getYear()],
  });
  let calenders = generateDaysArray();
  calenders = calenders.map((day) => {
    const result = {
      day,
    };
    const bookDay = bookings.filter((b) => b.day === day);
    if (bookDay.length === 0) {
      result.time_frames = timeFrames.map((tf) => {
        return {
          frame: tf,
          busy: [],
          free: subpitches,
        };
      });
    } else {
      result.time_frames = timeFrames.map((tf) => {
        const busySPs = bookDay.filter((bd) => bd.start_hour === tf[0] && bd.end_hour === tf[1]).map((bd) => bd.subpitch_id);
        return {
          frame: tf,
          busy: subpitches.filter((s) => busySPs.includes(s.subpitch_id)),
          free: subpitches.filter((s) => !busySPs.includes(s.subpitch_id)),
        };
      });
    }
    return result;
  });
  return calenders;
};
const getSubpitch = async (subpitch_id) => SubPitch.findOne({ where: { subpitch_id }, raw: true });
module.exports = {
  getPitchRegistration,
  approvePitchRegistration,
  registerPitch,
  getUserPitchRegistration,
  getPitchesPagination,
  getPitchesRegistrationPagination,
  getPitch,
  addSubPitch,
  getDetail,
  getBookingStatus,
  getSubpitch,
};

const { Role, UserRole } = require('../models');

const getRoleByName = (name) => Role.findOne({ where: { name } });
const createRoleForUser = (userId, roleId) => UserRole.create({ user_id: userId, role_id: roleId });
module.exports = {
  getRoleByName,
  createRoleForUser,
};

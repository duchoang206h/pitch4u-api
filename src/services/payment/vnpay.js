const querystring = require('qs');
const moment = require('moment');
const crypto = require('crypto');
const config = require('../../config/config');

process.env.TZ = 'Asia/Ho_Chi_Minh';
function sortObject(obj) {
  const sorted = {};
  const str = [];
  let key;
  // eslint-disable-next-line no-restricted-syntax
  for (key in obj) {
    // eslint-disable-next-line no-prototype-builtins
    if (obj.hasOwnProperty(key)) {
      str.push(encodeURIComponent(key));
    }
  }
  str.sort();
  for (key = 0; key < str.length; key++) {
    sorted[str[key]] = encodeURIComponent(obj[str[key]]).replace(/%20/g, '+');
  }
  return sorted;
}
const createPaymentUrl = ({ ipAddr, amount, bankCode, orderId }) => {
  const date = new Date();
  const createDate = moment(date).format('YYYYMMDDHHmmss');
  const { tmnCode, secretKey, returnUrl } = config.vnpay;
  let { vnpUrl } = config.vnpay;
  const locale = 'vn';

  const currCode = 'VND';
  // eslint-disable-next-line camelcase
  let vnp_Params = {};
  vnp_Params.vnp_Version = '2.1.0';
  vnp_Params.vnp_Command = 'pay';
  vnp_Params.vnp_TmnCode = tmnCode;
  vnp_Params.vnp_Locale = locale;
  vnp_Params.vnp_CurrCode = currCode;
  vnp_Params.vnp_TxnRef = orderId;
  vnp_Params.vnp_OrderInfo = `Thanh toan cho ma GD:${orderId}`;
  vnp_Params.vnp_OrderType = 'other';
  vnp_Params.vnp_Amount = amount * 100;
  vnp_Params.vnp_ReturnUrl = returnUrl;
  vnp_Params.vnp_IpAddr = ipAddr;
  vnp_Params.vnp_CreateDate = createDate;
  if (bankCode !== null && bankCode !== '') {
    vnp_Params.vnp_BankCode = bankCode;
  }

  // eslint-disable-next-line camelcase
  vnp_Params = sortObject(vnp_Params);

  const signData = querystring.stringify(vnp_Params, { encode: false });
  const hmac = crypto.createHmac('sha512', secretKey);
  // eslint-disable-next-line security/detect-new-buffer
  const signed = hmac.update(new Buffer(signData, 'utf-8')).digest('hex');
  vnp_Params.vnp_SecureHash = signed;
  // eslint-disable-next-line no-const-assign
  vnpUrl += `?${querystring.stringify(vnp_Params, { encode: false })}`;

  return vnpUrl;
};
const validatePaymentReturn = (vnp_Params) => {
  const secureHash = vnp_Params['vnp_SecureHash'];
  // eslint-disable-next-line no-param-reassign
  delete vnp_Params['vnp_SecureHash'];
  delete vnp_Params['vnp_SecureHashType'];
  vnp_Params = sortObject(vnp_Params);
  const { secretKey } = config.vnpay;
  const signData = querystring.stringify(vnp_Params, { encode: false });
  const hmac = crypto.createHmac('sha512', secretKey);
  const signed = hmac.update(new Buffer(signData, 'utf-8')).digest('hex');
  return secureHash === signed;
};

module.exports = {
  createPaymentUrl,
  validatePaymentReturn,
};

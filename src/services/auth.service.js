const httpStatus = require('http-status');
const tokenService = require('./token.service');
const userService = require('./user.service');
const { Token, VerificationCode } = require('../models');
const ApiError = require('../utils/ApiError');
const { tokenTypes, codeTypes } = require('../config/tokens');
const { genCode, verifyPassword, hashPassword } = require('../utils/hash');
const emailService = require('./email.service');
const verifyCodeService = require('./verification_code.service');

/**
 * Login with username and password
 * @param {string} email
 * @param {string} password
 * @returns {Promise<User>}
 */
const loginUserWithEmailAndPassword = async (email, password) => {
  const user = await userService.getUserByEmail(email);
  if (!user || !(await verifyPassword(password, user.password))) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Incorrect email or password');
  }
  user.password = ''; // hide pw
  return user;
};

/**
 * Logout
 * @param {string} refreshToken
 * @returns {Promise}
 */
const logout = async (refreshToken) => {
  const refreshTokenDoc = await Token.findOne({
    where: { token: refreshToken, type: tokenTypes.REFRESH, blacklisted: false },
  });
  if (!refreshTokenDoc) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Not found');
  }
  await refreshTokenDoc.destroy();
};

/**
 * Refresh auth tokens
 * @param {string} refreshToken
 * @returns {Promise<Object>}
 */
const refreshAuth = async (refreshToken) => {
  try {
    const token = await tokenService.verifyToken(refreshToken, tokenTypes.REFRESH);
    const user = await userService.getUserById(token.user_id);
    if (!user) {
      throw new ApiError(404, 'User not found');
    }
    await tokenService.removeToken(token.user_id, refreshToken, tokenTypes.REFRESH);
    return tokenService.generateAuthTokens(user);
  } catch (error) {
    if (error instanceof ApiError) throw error;
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Please authenticate');
  }
};

/**
 * Reset password
 * @param {string} resetPasswordToken
 * @param {string} newPassword
 * @returns {Promise}
 */
const resetPassword = async (userId, { password, new_password }) => {
  const user = await userService.getUserById(userId);
  if (user && !(await verifyPassword(password, user.password))) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Invalid password');
  }
  await userService.updateUserById(userId, { password: await hashPassword(new_password) });
};

/**
 * Verify email
 * @param {string} verifyEmailToken
 * @returns {Promise}
 */
const verifyEmail = async (userId, code) => {
  try {
    const storeCode = await verifyCodeService.getCode({ user_id: userId, code, type: codeTypes.VERIFY_EMAIL });
    if (!storeCode || storeCode.time <= 0) {
      throw new Error();
    }
    // todo: implement rate limit, time expires
    await verifyCodeService.deleteCode({ user_id: userId, type: codeTypes.VERIFY_EMAIL, code });
    await userService.updateUserById(userId, { is_verified: true });
  } catch (error) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Email verification failed');
  }
};
const sendVerifyEmail = async (user) => {
  const { user_id, email, is_verified } = user;
  if (is_verified) return;
  const code = genCode();
  await verifyCodeService.deleteCode({ user_id, type: codeTypes.VERIFY_EMAIL });
  await verifyCodeService.createCode({ user_id, code, type: codeTypes.VERIFY_EMAIL, time: 5 });
  emailService.sendVerificationEmail(email, code);
};
const sendEmailResetPassword = async (user) => {
  // eslint-disable-next-line camelcase
  const { email, user_id, is_verified } = user;
  if (!is_verified) return;
  const code = genCode();
  await verifyCodeService.deleteCode({ user_id, type: codeTypes.RESET_PASSWORD });
  await verifyCodeService.createCode({ user_id, code, type: codeTypes.RESET_PASSWORD, time: 5 });
  emailService.sendResetPasswordEmail(email, code);
};
const verifyForgotPassword = async (email, code) => {
  const user = await userService.getUserByEmail(email);
  const storeCode = await verifyCodeService.getCode({ user_id: user.user_id, code, type: codeTypes.RESET_PASSWORD });
  if (!storeCode || storeCode.time <= 0) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'Invalid code');
  }
};
const forgotPassword = async ({ email, code, password }) => {
  await verifyForgotPassword(email, code);
  const user = await userService.getUserByEmail(email);
  await verifyCodeService.deleteCode({ user_id: user.user_id, code, type: codeTypes.RESET_PASSWORD });
  await userService.updateUserById(user.user_id, { password: await hashPassword(password) });
};
module.exports = {
  loginUserWithEmailAndPassword,
  logout,
  refreshAuth,
  resetPassword,
  verifyEmail,
  sendVerifyEmail,
  sendEmailResetPassword,
  verifyForgotPassword,
  forgotPassword,
};

const nodemailer = require('nodemailer');
const config = require('../config/config');
const logger = require('../config/logger');
const { getMailTemplate } = require('./mail-templates');

const transport = nodemailer.createTransport(config.email.smtp);
/* istanbul ignore next */
if (config.env !== 'test') {
  transport
    .verify()
    .then(() => logger.info('Connected to email server'))
    .catch(() => logger.warn('Unable to connect to email server. Make sure you have configured the SMTP options in .env'));
}

/**
 * Send an email
 * @param {string} to
 * @param {string} subject
 * @param {string} text
 * @returns {Promise}
 */
const sendEmail = async (to, subject, text) => {
  try {
    const msg = { from: config.email.from, to, subject, html: text };
    await transport.sendMail(msg);
  } catch (error) {
    console.log(error);
  }
};

/**
 * Send reset password email
 * @param {string} to
 * @param {string} token
 * @returns {Promise}
 */
const sendResetPasswordEmail = async (to, code) => {
  const content = getMailTemplate('resetPassword');
  await sendEmail(to, content.subject, content.html({ code }));
};

/**
 * Send verification email
 * @param {string} to
 * @param {string} token
 * @returns {Promise}
 */
const sendVerificationEmail = async (to, code) => {
  const content = getMailTemplate('emailVerification');
  await sendEmail(to, content.subject, content.html({ code }));
};
exports.transport = transport;
exports.sendEmail = sendEmail;
exports.sendResetPasswordEmail = sendResetPasswordEmail;
exports.sendVerificationEmail = sendVerificationEmail;

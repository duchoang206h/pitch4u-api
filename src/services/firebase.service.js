const admin = require('firebase-admin');
const serviceAccount = require('../config/serviceAccount.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});
const auth = admin.auth();

const sendOtpVerification = async (to) => {
  return new Promise((resolve, reject) => {
    auth
      .sendSignInLinkToPhone(to)
      .then((verificationId) => {
        resolve(verificationId);
      })
      .catch((error) => {
        reject(error);
      });
  });
};
const verifyOtp = async (verificationId, code) => {
  return new Promise((resolve, reject) => {
    auth
      .signInWithPhoneNumber(verificationId, code)
      .then(() => {
        resolve(true);
      })
      .catch((error) => {
        reject(error);
      });
  });
};
sendOtpVerification('+84338650408').then();
module.exports = {
  sendOtpVerification,
  verifyOtp,
};

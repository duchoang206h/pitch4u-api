const { VerificationCode } = require('../models');

const createCode = async (data) => VerificationCode.create(data);
const deleteCode = async (where) => {
  return VerificationCode.destroy({ where });
};
const updateCode = async (where, updates) => {
  return VerificationCode.update(updates, { where });
};
const getCode = async (where) => VerificationCode.findOne({ where, raw: true });
module.exports = {
  createCode,
  updateCode,
  deleteCode,
  getCode,
};

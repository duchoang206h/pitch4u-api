const moment = require('moment');
const { BOOKING_STATUS, PAYMENT_TYPE, TRANSACTION_STATUS } = require('../config/constant');
const { sequelize, SubPitch, Booking, BookingPitch, Pitch, Transaction, User, Review } = require('../models');
const ApiError = require('../utils/ApiError');
const { getOffset } = require('../utils/common');
const { createPaymentUrl } = require('./payment/vnpay');
const pitchService = require('./pitch.service');
const logger = require('../config/logger');
const { Op } = require('sequelize');

const booking = async ({ subpitch_id, user_id, start_time, end_time, payment_type, voucher_id = null, ipAddr }) => {
  // todo: implement voucher

  const t = await sequelize.transaction();
  const hours = moment(end_time).diff(moment(start_time), 'hours');
  const subpitch = await pitchService.getSubpitch(subpitch_id);
  const total = subpitch.price * hours;
  try {
    const existConcurrentBooking = await BookingPitch.findOne({
      where: {
        subpitch_id,
        start_time: new Date(start_time),
        end_time: new Date(end_time),
      },
      transaction: t,
      lock: true,
    });
    if (existConcurrentBooking) throw new ApiError(400, 'Booking existed');
    const bookInfo = await Booking.create(
      {
        user_id,
        payment_type,
        status: BOOKING_STATUS.PENDING,
        total,
        pitch_id: subpitch.pitch_id,
        booking_pitches: [
          {
            subpitch_id,
            start_time: new Date(start_time),
            end_time: new Date(end_time),
            price: subpitch.price,
          },
        ],
      },
      { include: [{ model: BookingPitch, as: 'booking_pitches' }], transaction: t }
    );
    await t.commit();
    // eslint-disable-next-line camelcase
    let paymentUrl = '';
    // eslint-disable-next-line camelcase
    if (payment_type === PAYMENT_TYPE.VNPAY) {
      paymentUrl = createPaymentUrl({
        ipAddr,
        amount: bookInfo.total,
        bankCode: '',
        orderId: bookInfo.booking_id,
      });
    }
    return {
      bookInfo,
      paymentUrl,
    };
  } catch (error) {
    await t.rollback();
    if (error instanceof ApiError) throw error;
    else throw new ApiError(500, 'Internal server error');
  }
};
const getUserBookings = async (user_id, { page, limit }) => {
  const offset = getOffset(page, limit);
  const bookings = await Booking.findAndCountAll({
    where: {
      user_id,
    },
    // todo: select only needed fields
    include: [
      {
        model: BookingPitch,
        as: 'booking_pitches',
        include: [
          {
            model: SubPitch,
            as: 'sub_pitch',
          },
        ],
      },
      {
        model: Pitch,
        as: 'pitch',
        attributes: {
          include: [
            [
              sequelize.fn(
                'COALESCE',
                sequelize.literal('(SELECT AVG(star) FROM reviews WHERE pitch_id = pitch.pitch_id)'),
                0
              ),
              'rate',
            ],
          ],
        },
        include: [
          {
            model: User,
            as: 'user',
          },
        ],
      },
    ],
    limit,
    offset,
    order: [['createdAt', 'desc']],
  });
  return {
    data: bookings.rows,
    total: bookings.count,
    page,
    limit,
  };
};
const validateTransactionReturn = async ({ booking_id, amount, tx_code, extra_data = {} }) => {
  const bookingInfo = await Booking.findByPk(booking_id);
  const transaction = await Transaction.findOne({ where: { booking_id, status: TRANSACTION_STATUS.SUCCESS } });
  if (transaction) return;
  let error = '';
  let status = TRANSACTION_STATUS.PENDING;
  if (!bookingInfo) return;
  if (parseFloat(bookingInfo.total) !== amount) {
    error = 'Invalid amount';
    status = TRANSACTION_STATUS.ERROR;
  } else {
    status = TRANSACTION_STATUS.SUCCESS;
  }
  const t = await sequelize.transaction();
  try {
    if (status === TRANSACTION_STATUS.SUCCESS) {
      bookingInfo.status = BOOKING_STATUS.SUCCESS;
      await bookingInfo.save({ transaction: t });
    }
    await Transaction.create(
      {
        booking_id,
        status,
        error,
        data: extra_data,
        total: amount,
        tx_code,
        type: PAYMENT_TYPE.VNPAY,
      },
      { transaction: t }
    );
    await t.commit();
  } catch (error) {
    logger.error(`VNP/RETURN error:: ${JSON.stringify(error)}`);
    await t.rollback();
  }
};
// eslint-disable-next-line camelcase
const getBookingPaginate = async ({ limit, page, name, start_date, end_date, status, payment_type, user_id }) => {
  const bookingFilter = [];
  const pitchFilter = [];
  if (name) pitchFilter.push({ name: { [Op.like]: `%${name}%` } });
  // eslint-disable-next-line camelcase
  if (start_date)
    bookingFilter.push({
      createdAt: {
        [Op.gte]: start_date,
      },
    });
  // eslint-disable-next-line camelcase
  if (end_date) {
    bookingFilter.push({
      createdAt: {
        [Op.lte]: end_date,
      },
    });
  }
  if (status) bookingFilter.push({ status });
  // eslint-disable-next-line camelcase
  if (payment_type) bookingFilter.push({ payment_type });
  // eslint-disable-next-line camelcase
  if (user_id) bookingFilter.push({ user_id });
  const bookings = await Booking.findAndCountAll({
    where: {
      [Op.and]: bookingFilter,
    },
    include: [
      {
        model: Pitch,
        where: {
          [Op.and]: pitchFilter,
        },
      },
      {
        model: User,
        as: 'user',
      },
      {
        model: BookingPitch,
        as: 'booking_pitches',
        include: [
          {
            model: SubPitch,
            as: 'sub_pitch',
          },
        ],
      },
    ],
    limit,
    offset: getOffset(page, limit),
  });
  return {
    data: bookings.rows,
    total: bookings.count,
    page,
    limit,
  };
};
module.exports = {
  booking,
  getUserBookings,
  validateTransactionReturn,
  getBookingPaginate,
};

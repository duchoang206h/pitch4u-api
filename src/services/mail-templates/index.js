const emailVerificationHtml = require('./emailVerfication');
const emailResetPasswordHtml = require('./resetPassword');

const templates = [
  {
    type: 'emailVerification',
    contents: [
      {
        lang: 'vi',
        html: emailVerificationHtml,
        subject: 'Xác thực địa chỉ email',
      },
    ],
  },
  {
    type: 'resetPassword',
    contents: [
      {
        lang: 'vi',
        html: emailResetPasswordHtml,
        subject: 'Đổi mật khẩu',
      },
    ],
  },
];
const getMailTemplate = (type, lang = 'vi') => {
  const template = templates.find((t) => t.type === type);
  if (!template) throw new Error('Email template not found');
  const content = template.contents.find((c) => c.lang === lang);
  return content;
};
module.exports = {
  getMailTemplate,
};

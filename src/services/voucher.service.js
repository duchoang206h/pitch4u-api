const { VOUCHER_TYPES } = require('../config/constant');
const { Voucher } = require('../models');
const ApiError = require('../utils/ApiError');

const createVoucher = async (userId, pitchId, data) => {
  const { type, value, expire_date, limit_time, max_amount, code } = data;
  return Voucher.create({
    pitch_id: pitchId,
    created_by: userId,
    created_from: 'user',
    type,
    value,
    expire_date,
    limit_time,
    max_amount,
    code,
  });
};
const getVoucherByCode = async (code) => Voucher.findOne({ where: { code } });

const getVoucherByPitch = async (pitchId) => Voucher.findAll({ where: { pitch_id: pitchId } });

const updateVoucher = async (voucherId, data) => Voucher.update(data, { where: { voucher_id: voucherId } });
const deleteVoucher = async (voucherId) => Voucher.destroy({ where: { voucher_id: voucherId } });
module.exports = {
  createVoucher,
  getVoucherByCode,
  getVoucherByPitch,
  deleteVoucher,
  updateVoucher,
};

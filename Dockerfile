FROM node:16.20-alpine as builder
WORKDIR /app
COPY package.json .
RUN npm install
FROM node:16.20-alpine
WORKDIR /app
COPY --from=builder /app .
COPY . .
CMD [ "node", "src/index.js" ]